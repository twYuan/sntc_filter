# README

### What is this repository for?

此程式可將自動將openaudit撈回的device列表對SNTC Server進行filter error的動作

### How do I run it?

1. 安裝 node-red

wget https://nodejs.org/dist/v12.19.0/node-v12.19.0-linux-x64.tar.xz
sudo mkdir -p /usr/local/lib/nodejs
sudo tar -xJvf node-v12.19.0-linux-x64.tar.xz -C /usr/local/lib/nodejs

2. 修改環境變數

vi ~/.bash_profile
PATH=/usr/local/lib/nodejs/node-v12.19.0-linux-x64/bin/:$PATH

3. 測試

node -v
npm version
npx -v

4. 安裝newman

npm install -g newman

5. 執行

newman run sntc_filter.postman_collection.json -r cli,json -n 50 -e SNTC.postman_environment.json 



### Reference

https://nodejs.org/en/download/
https://www.footmark.info/linux/centos/centos7-setting-nodejs-yum/
https://www.npmjs.com/package/newman
